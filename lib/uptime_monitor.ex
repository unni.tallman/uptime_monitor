defmodule UptimeMonitor do
  def check_url(url) do
    {_, response} = HTTPoison.get(url, [], [ ssl: [{:versions, [:'tlsv1.2']}] ])
    
    Map.has_key?(response, :status_code) && (response.status_code == 200)
  end

  def check_urls(urls) do 
      urls
      |> Task.async_stream(&check_url/1)
      |> Enum.map(fn {:ok, res} -> res end)
  end

  def get_results(urls) do
    results = check_urls(urls)
    results
    |> Enum.with_index
    |> Enum.map(fn({x, i}) ->
      {Enum.at(urls, i),x}
    end)
    |> Map.new
  end

  def monitor do 
    Site.get_url_list |> get_results |> Site.update_sites_status
  end

  def main([]) do
    monitor()
  end
end
