defmodule Site do 
  def get_url_list do 
    res = database_client("SELECT url FROM sites")
    Enum.map(res.rows, fn x -> Enum.at(x,0) end)
  end

  def update_sites_status(sites_statuses) do 
    Enum.each(sites_statuses, fn {url, value} -> 
      query = 
        if(value) do 
          "UPDATE sites SET up = #{value}, updated_at=now() WHERE url = '#{url}'"
        else
          "UPDATE sites SET up = #{value}, updated_at=now() WHERE url = '#{url}'"
        end
        
      database_client(query)
    end)
  end

  defp database_client(query) do 
    db_host = Application.get_env(:uptime_monitor, :database_host)
    db_name = Application.get_env(:uptime_monitor, :database_name)
    db_user = Application.get_env(:uptime_monitor, :database_user)
    db_password = Application.get_env(:uptime_monitor, :database_password)

    {:ok, pid} = Postgrex.start_link(hostname: db_host, username: db_user, password: db_password, database: db_name)
    Postgrex.query!(pid, query, [])
  end
end
