# UptimeMonitor

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `uptime_monitor` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:uptime_monitor, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/uptime_monitor](https://hexdocs.pm/uptime_monitor).

## Build

```
  mix deps.get
  MIX_ENV=prod mix escript.build
```

## Console

```
  iex -S mix
```
